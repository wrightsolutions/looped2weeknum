#!/usr/bin/env python
#
# Copyright (c) 2017, Gary Wright http://www.wrightsolutions.co.uk/contact
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the copyright holder nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Extract summary information from Slurm
Output to an sqlite db.
minspause parameter is the number of MINUTES to sleep between samples (default 6)
--pathstem to specify the directory to which we should output the sample data

Example of how to specify /var/log/slm as the place to put the weeknum suffixed output
  looped2week_slm.py -m 6 -p /var/log/slm
Inspection of the output using: sqlite3 /var/log/slm/spart__201748.sqlite

For much feedback on error before running: export PYVERBOSITY=2

Tested initially using Python 2.7.5
"""

from __future__ import print_function
from __future__ import with_statement

from os import path,sep
from os import getenv as osgetenv
#from os import path as ospath
from sys import argv
#import logging
from datetime import date
from datetime import datetime as dt
import re
import shlex
import sqlite3 as db3
#from string import printable
from subprocess import Popen,PIPE
from sys import argv,exit
import time


from string import ascii_letters,digits,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE=set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))
TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3
STATE_DEPENDENT=4
STATE_UNKNOWN_MESSAGE='STATE UNKNOWN'
TIMEOUT_DEFAULT=24
DESCRIPTION='Slurm status sampling'

try:
	from argparse import ArgumentParser, RawTextHelpFormatter
except ImportError:
	print(STATE_UNKNOWN_MESSAGE)
	exit(STATE_UNKNOWN)

VERSION='0.1'

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

COLON=chr(58)
DOTTY = chr(46)
BRACE_OPEN=chr(123)
BRACE_CLOSE=chr(125)
# Above 12x are curly {}, below 4x are rounded so ()
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
#RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')



#RE_PALPHA = re.compile('%[a-z]+',re.IGNORECASE)
RE_PALPHA = re.compile('%[a-zA-Z]+')
RE_INT10 = re.compile('[0-9]{1,10}')
RE_TIME6 = re.compile('[0-9]{2}:[0-9]{2}:[0-9]{2}')
TEXT_FIELD_FALLBACK = 'No detection worked so far so trying str() fallback'
SPART_MIN = 5
SPART_MAX = 600

CMD_SINFO='usr/bin/sinfo --sort=+P,-t'

STRING_SUBOUT_ARGS_INVALID='Args list for subout is not valid - does it contain pipe (|)?'
STRING_NTH_ARG_REGEX_FAILED_TEMPLATE="Arg {0} did not pass the required regex test!"
STRING_NONZERO_RC_TEMPLATE="Command {0} gave non-zero returncode rc={1}"

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
#MEG_AS_FLOAT = float(2**20)
""" PARTITION   AVAIL  TIMELIMIT  NODES  STATE NODELIST
partstate is a concatenation of partition name+state
Example: long queue+mix would be 'longmix'
This leaves fields NODES,TIMELIMIT,NODELIST,AVAIL
"""
SQLSTRING_CREATE_SPART = ("CREATE TABLE IF NOT EXISTS"
" spart(partstate text NOT NULL,"
" ncount integer NOT NULL, "
" timelimit text NOT NULL, "
" nlist text, "
" nixtime_created real NOT NULL,"
" sampled_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
" isotimeutc_created text,"
" avail text,"
" PRIMARY KEY(partstate,ncount,sampled_at)"
" );")
SLEEPSECS = 360
TWO_TO_TWENTYTWO = 2**22

dbcon = None

dbfound_flag = False
tablefound_flag = False
date_today = date.today()
isoformat_now = dt.now().isoformat()
yyyyww = "{0}{1:0>2}".format(date_today.year,date_today.isocalendar()[1])


class ArgParser(ArgumentParser):
	def error(self, message):
		#self.print_help()
		print(STATE_UNKNOWN_MESSAGE, ': ', message)
		exit(STATE_UNKNOWN)


def parser_initialise():
	try:
		par = ArgParser(description=DESCRIPTION,formatter_class=RawTextHelpFormatter)
	except:
		par = None

	if par is None:
		return par

	par.add_argument('-V', '--version', help='Show plugin version',
		action='store_true')
	par.add_argument('-p', '--pathstem', help='Path stem to use when creating output store',
		action='store', dest='pathstem', default='/tmp', required=False)
	par.add_argument('-s', '--stype', help='Slurm type to query (info / queue)',
		action='store', dest='stype', default=None, required=False)
	par.add_argument('-l', '--linesmax', help='Maximum number of lines to process',
		action='store', dest='linesmax', type=int, default=100000)
	par.add_argument('-m', '--minspause', help='Minutes to pause between each sample.',
		action='store', dest='minspause', type=int, default=6)
	par.add_argument('-t', '--timeout', help='(Sub)Process timeout (where input not piped in)',
		action='store', dest='timeout', type=int, default=TIMEOUT_DEFAULT)
	par.add_argument('files', metavar='FILE', nargs='*', help='files of Slurm information / queue')

	return par


def connect_get_cursor(database_path,verbosity=0):
	global dbcon
	cur = None
	isoformat_now = dt.now().isoformat()
	if path.exists(database_path):
		if verbosity > 0:
			print("{0} Attempting append to sqlite db at {1}.".format(
				isoformat_now,database_path))
		try:
			dbcon = db3.connect(database_path,timeout=2)
		except db3.Error as e:
			print('Sqlite connection error occurred: %s', e.args[0])
			return None
		except Exception as e:
			print('Sqlite connection error: %s', e)
			return None
		dbcon.isolation_level = None
		cur = dbcon.cursor()
	else:
		dbcon = db3.connect(database_path)
		if verbosity > 1:
			print("{0} Creating initial sqlite db at {1}.".format(
			isoformat_now,database_path))
			if verbosity > 2:
				print(SQLSTRING_CREATE_SPART)
		dbcon.execute(SQLSTRING_CREATE_SPART)
		cur = dbcon.cursor()
	return cur


def create_table(cur):
    """ Having a cursor object does not guarantee that the file was an sqlite
    database. A plain text file get a cursor, but fails on cur.execute() """
    global tablefound_flag

    #if debug is True:
    #    isoformat_now = dt.now().isoformat()
    #    print("{0} Creating target table in the sqlite db.".format(isoformat_now))

    try:
        #print(SQLSTRING_CREATE_SPART)
        cur.execute(SQLSTRING_CREATE_SPART)
        tablefound_flag = True
    except db3.ProgrammingError as e:
        err_str = "Sqlite execute() ProgrammingError occurred: {0}".format(e)
        print(err_str)
        cur.close()
        return False
    except db3.Error as e:
        err_str = "Sqlite execute() create table error {0}: {1}".format(
            database_path, e.args[0])
        print(err_str)
        cur.close()
        return False
    except:
        err_str = "Sqlite error during .execute() create if not exists."
        print(err_str)
        cur.close()
        return False
    return True


def insert_sample(sample_tuple,cur=None):

    global tablefound_flag

    if not tablefound_flag:
        create_table(dbcon.cursor())

    #print("Next we insert into the procmem table.")
    try:
        if cur is None:
            curinsert = dbcon.cursor()
        else:
            curinsert = cur
        #print(sample_tuple[1])
        curinsert.execute("""INSERT INTO spart (partstate,ncount,timelimit,nlist,
nixtime_created,sampled_at,isotimeutc_created,avail)
 VALUES(?,?,?,?,?,?,?,?);
""",sample_tuple)
    except db3.OperationalError as e:
        err_str = 'Sqlite execute() OperationalError occurred: {0}'.format(e)
        print(err_str)
        if cur is None:
            curinsert.close()
        return False
    except db3.ProgrammingError as e:
        err_str = 'Sqlite execute() ProgrammingError occurred: {0}.format(e)'
        print(err_str)
        if cur is None:
            curinsert.close()
        return False
    except db3.Error as e:
        err_str = 'Sqlite execute() error occurred: {0}'.format(e)
        print(err_str)
        if cur is None:
            curinsert.close()
        curinsert.close()
        return False
    except ValueError as e:
        err_str = 'Sqlite insert failed ValueError: {0}'.format(e)
        print(err_str)
        if cur:
            curinsert.close()
        curinsert.close()
        return False
    except Exception as e:
        err_str = 'Sqlite insert failed: {0}'.format(e)
        print(err_str)
        if cur:
            curinsert.close()
        curinsert.close()
        return False

    if cur is None:
        curinsert.close()
    return True


def looped_sampling3(cur=None,dbpath=None,sleepmins=6):
    """ 1440 minutes in a day. 240 iterations per day if you are sleeping
    for 6 minutes. Setting a 100 day limit for sleep(60) would be 100*1440
    but setting a 100 day limit for sleep(360) would be 100*240 which is 24000
    """

    global database_path

    idx_upper = 100*(1440/sleepmins)

    process3state = None

    if dbpath is None:
        return process3state

    idx = 0
    sample_tuple = ()
    while idx < idx_upper:
        try:
            nixtime_created = int(time.time())
            isotimeutc_created = dt.isoformat(dt.utcfromtimestamp(nixtime_created))
            #isocompact = dt.now().strftime(ISOCOMPACT_STRFTIME)
        except AttributeError:
            print("AttributeError - is the correct version of all libraries installed?")
            break
        except:
            """ The process has not survived all iterations of the while loop """
            break

        sample_tuple = (partstate,ncount,timelimit,nlist,
            nixtime_created,sampled_at,isotimeutc_created,avail)

        insert_sample(sample_tuple,cur)

        process3state = False
        #print(sample_tuple)
        idx += 1
        try:
            cur.execute("commit;")
        except:
            pass
        time.sleep(SLEEPSECS)
        # Sleep is the last thing we do in this loop iteration
    else:
        # We completed all iterations - the process survived
        # return idx_upper
        pass

    return idx


def lines_joined(lines,joiner=chr(10)):
	""" chr(9) is tab  ; chr(10) is line feed ;
	chr(32) is space ; chr(61) is equals (=)
	"""
        joined = joiner.join(lines)
        return joined


def empty_if_unprintables(unfiltered):
	#stripped = unfiltered.strip()
	if set(unfiltered).issubset(SET_PRINTABLE):
		return unfiltered
	return ''


def joined_from_list(arglist,joinstr=' '):
	return joinstr.join([str(arg) for arg in arglist])


def dirname_split_lower(dirname,lowcase=True):
	dsplit = dirname.split('_')[-1]
	if len(dsplit) > 0:
		dres = str.lower(dsplit)
	else:
		dres = dsplit
	return dres


def args_valid(args_list):
	pipe_count = 0
	for arg in args_list:
		try:
			if PIPEY in arg:
				pipe_count+=1
				break
		except TypeError:
			pass

	return (pipe_count < 1)


def args_valid3(args=[], argnum=0, arg_regex=None):
	feedback_line = ''
	arg=args[argnum]
	#print("Testing arg={0}".format(arg))
	valid3rc = 0
	if arg_regex.match(arg):
		pass
	else:
		# 1+argnum so report back to the user is more natural (one indexed)
		feedback_line = STRING_NTH_ARG_REGEX_FAILED_TEMPLATE.format(1+argnum)
		valid3rc = 204
	return (valid3rc,feedback_line)


def subout2match(command_without_args, args=[]):
	""" Suffix of 2 as we accept two args generally.
	cmdline should NOT contain any chr(124) pipe symbols (|)
	"""
	lines_feedback = []
	command_with_args = None
	s_rc = 0
	if args_valid(args):
		pass
	else:
		lines_feedback = [STRING_SUBOUT_ARGS_INVALID]
		s_rc = 202

	if s_rc > 0:
		return (s_rc,[],[],lines_feedback,command_with_args)

	command_with_args = "{0} {1}".format(command_without_args,joined_from_list(args,' '))
	if PYVERBOSITY > 1:
		print(command_with_args)
	cmdproc = Popen(shlex.split(command_with_args),bufsize=BUFSIZE1, \
		stdout=PIPE,stderr=PIPE)
	lines = []
	for line in cmdproc.stdout:
		lines.append(line)
		#print(line)

	cmdproc.wait()
	s_rc = cmdproc.returncode
	if s_rc > 0:
		feedback_rc = STRING_NONZERO_RC_TEMPLATE.format(command_without_args,s_rc)
		lines_feedback.append(feedback_rc)
		if 1==s_rc and CMD_SINFO==command_without_args:
			lines_feedback.append(FEEDBACK_PERMS_EVERY_FILE)
	#print(cmdproc.returncode)
	return (s_rc,lines,[],lines_feedback,command_with_args)


if __name__ == "__main__":

	parser = parser_initialise()
	if parser is None:
		print(STATE_UNKNOWN_MESSAGE)
		exit(STATE_UNKNOWN)
	""" parse_args() default method exits status code 2
	but our local override to error() ensures argument
	problems get a STATE_UNKNOWN which is likely 3 (see constants) """
	args = parser.parse_args()

	""" Have reached this far so arguments expected by program look okay """
	if args.version:
		print('Plugin version is', VERSION)
		exit(STATE_OK)

	inputmode = None

	if args.files:
		inputmode = 'filelist'
	elif args.timeout is None:
		# Likely no -t switch given so assume stdin can be read in
		pass
	else:
		# User has supplied timeout
		inputmode = 'subprocess'
		if args.timeout < 1:
			""" Zero or negative should be caught and when happens use default """
			args.timeout = TIMEOUT_DEFAULT

	""" By now we have inputmode set as 'filelist', 'subprocess' or something else 
	where filelist indicates the user has named the files to process
	and subprocess means the program should query using direct commands
	and process the output automatically
	"""

	program_binary = argv[0].strip()

	# Next set stype spart unless logic dictates otherwise
	if args.stype:
		stype = args.stype
	elif 'subprocess' == inputmode:
		stype = 'spart'
	else:
		stype = 'spart'

	exit_rc = STATE_OK
	minspause_str = None
	pathstem_given = None
	pathstem_expanded = path.expanduser('~')

	if args.pathstem is None or len(args.pathstem) < 2:
		if PYVERBOSITY > 1:
			print(".sqlite file will be created in {0} by default.".format(pathstem_expanded))
		database_path = '{0}{1:<1}spart__{2}.sqlite'.format(
			pathstem_expanded,sep,yyyyww)
	else:
		if PYVERBOSITY > 1:
			print(".sqlite file will be created in {0} as requested.".format(args.pathstem))
		database_path = '{0}{1:<1}spart__{2}.sqlite'.format(
			args.pathstem,sep,yyyyww)

	if PYVERBOSITY > 1:
		print("{0} any database logging will go to {1}".format(isoformat_now,database_path))

	#flines = fileinput_lines(fileinput.input(files=args.files)

	"""
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		if PYVERBOSITY > 1:
	"""

	survived3state = None
	try:
		cur = connect_get_cursor(database_path,(PYVERBOSITY>1))
		if 0 == args.minspause:
			# Onetime run, likely from cmdline or cron
			lines_out = []
			lines_feedback = []
			"""
			arg_regex_rc,feedback_line = args_valid3(mkdir_argslist, 2, RE_ALPHA_UNDERSCORE)
			if arg_regex_rc > 0:
				sub_rc = arg_regex_rc
				lines_feedback.append(feedback_line)
			"""

			if 0 == sub_rc:
				sinfo_argslist = ['--local']
				sub_rc, lines_out, lines_err, lines_feedback, _ = \
					subout2match(CMD_SINFO, sinfo_argslist)

			slines = []
			if 0 == sub_rc:
				slines = lines_out
			else:
				if PYVERBOSITY is None or PYVERBOSITY > 0:
					if PYVERBOSITY > 1 and len(lines_feedback) > 0:
						for line in lines_feedback:
							print(line)
				print("Exiting {0}".format(sub_rc))
				exit(sub_rc)

			rows_inserted = spart_inserts(cur,database_path,slines)
			loop_iterations = 1
		elif args.minspause < TWO_TO_TWENTYTWO:
			loop_iterations = looped_sampling3(cur,database_path,args.minspause)
		else:
			loop_iterations = looped_sampling3(cur,database_path)
	except:
		raise
		exit(141)

	if dbcon:
		cur = dbcon.cursor()
		try:
			cur.execute("commit;")
		except:
			pass
		dbcon.close()

	if loop_iterations < 1:
		exit(151)





	if sub_rc > 0:
		exit(sub_rc)

	#chmod(dir2supplied, CHMOD2)

	exit(sub_rc)

