#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import path,sep
from os import stat
from sys import argv
import datetime
import re
import time

from string import ascii_letters,digits,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE = set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))

try:
    import MySQLdb as dbm
except ImportError:
    print("Is MySQL-python (Mysql/mariadb connector) installed?")
    # apt-get install python3-mysqldb
    # yum install MySQL-python
    exit(191)

try:
    import datadog
except:
    print("Is datadog installed? (in a python3 virtualenv) ?")
    # Want the same functionality as gcp.cloudsql.database.disk.write_ops_count? Use 'type': 'count'
    exit(192)

# python3 ./looped_sample2datadog_directory_driven dbname tname '2020-03-31'

ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'

TABLECHECK_FROM='SELECT TABLE_NAME FROM information_schema.TABLES '

#date_today = date.today()
#isoformat_now = dt.now().isoformat()
#yyyyww = "{0}{1:0>2}".format(date_today.year,date_today.isocalendar()[1])
#yyyy = ''+str(date_today.year)
M_QUERY="""SELECT Count(*) FROM
WHERE created_date > '{1}'
GROUP BY {0}.eventlog.user_id
;
"""


def isfile_and_positive_size(target_path_given):
    from os import path as pth
    target_path = target_path_given.strip()
    try:
        if not pth.isfile(target_path):
            return False
    except IOError as e:
        return False

    from os import stat
    try:
        if not stat(target_path).st_size:
            return False
    except IOError as e:
        return False
    return True


def get_connection(connect_dict, ssl_dict=''):
    return dbm.connect(**connect_dict, ssl=ssl_dict)


def get_connection_and_cursor(connect_dict, ssl_dict):
    con = get_connection(connect_dict, ssl_dict)
    cur = con.cursor()
    return con, cur


def close_connection(con):
    try:
        con.close()
    except:
        # Don't care about error
        pass


def close_cursor(cur):
    try:
        cur.close()
    except:
        # Don't care about error
        pass


def close_connection_and_cursor(con, cur):
    close_cursor(cur)
    close_connection(con)


def has_table_m(cursor, table_name, verbosity=0):
    tablefound_flag = False
    if table_name is not None:
        # TW is a short alias for TABLE WHERE
        TW="WHERE TABLE_NAME='{0}' AND TABLE_SCHEMA=database();".format(table_name)
        tablecheck = TABLECHECK_FROM+TW
        print(tablecheck)
        try:
            cursor.execute(tablecheck)
        except dbm.OperationalError:
            raise
        except:
            print('Unknown error during tablecheck')
            raise
        row = cursor.fetchone()
        tname = row[0]
        if verbosity > 1 and tname:
            print("InnoDB/MyISAM Database does have table named {0}".format(table_name))
        if tname:
            tablefound_flag = True
        return tablefound_flag


def read_sample(cur, m_query, debug=False):
    try:
        cur.execute(m_query)
        row_tup = cur.fetchone()
        if debug == True:
            print(row_tup)
        try:
            q_count = int(row_tup[0])
            q_count = int(datetime.datetime.now().strftime('%S'))
        except OverflowError:
            print("count too large!")
            raise
        except:
            raise
    except dbm.OperationalError:
        raise
    except:
        print('Unknown error during fetch of count')
        raise

    return q_count


if __name__ == "__main__":

    interval_seconds=3600
    DEBUG = False

    datadog_options = {
        'api_key': '3f',
        'host_name': 'icinga2'
    }
    datadog.initialize(**datadog_options)

    con = None
    cur = None
    dbname = None
    tname = None
    date_start = '1970-01-01'
    if (len(argv) < 2):
        print('Expects at least 3 args. First arg is dbname. Second is table name. Third is interval (minutes).')
        exit(141)

    if len(argv) > 1:
        dbname = argv[1]
        if len(argv) > 2:
            tname = argv[2]
            if len(argv) > 3:
                interval_mins = argv[3]

    if DEBUG is True:
        #print("Arguments in use: db={0} ; table={1} ; date_start={2}".format(dbname, tname, date_start))
        print("Arguments in use: db={0}, table={1}, interval_mins={2}".format(dbname, tname, interval_mins))
        interval_seconds = 10
    else:
        #interval_seconds = 60*int(interval_mins)
        interval_seconds = 60*5		# fixed at 5 minutes

    SSL_STEM='/home/datadog/mysqlssl'
    mca=SSL_STEM+'/mysql_ca.pem'
    mcert=SSL_STEM+'/mysql_cert.pem'
    mkey=SSL_STEM+'/mysql_key.pem'
    ssl_dict = {'ca': mca,
    'cert': mcert,
    'key': mkey
    }

    if not isfile_and_positive_size(mca):
        print('mca file is missing!')
        exit(111)
    if not isfile_and_positive_size(mcert):
        print('mcert file is missing!')
        exit(121)
    if not isfile_and_positive_size(mkey):
        print('mkey file is missing!')
        exit(131)
    if dbname is None:
        print('Unspecified dbname!')
        exit(141)

    connect_dict= { 'host': '127.0.0.1',
        'user': 'back',
        'passwd': 'redactREDACT',
        'db': dbname
    }

    if DEBUG or 1>0:
        print(ssl_dict)

    try:
        con, cur = get_connection_and_cursor(connect_dict, ssl_dict)
    except dbm.OperationalError:
        if 'host' in connect_dict:
            print("+++ Unable to connect to mysql/mariadb on host {0}".format(connect_dict['host']))
        raise
    except:
        raise

    tfound_count = 0
    if has_table_m(cur, tname, 2):
        tfound_count += 1

    close_connection_and_cursor(con, cur)

    if DEBUG or 1>0:
        print(tfound_count)
    sampling_verbosity=2

    iter_count = 0
    while True:
        q_count = 0
        try:
            con, cur = get_connection_and_cursor(connect_dict, ssl_dict)
            time_now = datetime.datetime.now()
            start_time = time_now - datetime.timedelta(minutes=5)
            time_sqlformatted = start_time.strftime("%Y-%m-%d %H:%M:%S")
            m_query = M_QUERY.format(dbname,time_sqlformatted)

            if DEBUG == True:
                print(m_query)

            q_count = read_sample(cur, m_query, DEBUG)
            close_connection_and_cursor(con, cur)
            iter_count += 1

            # 'type': 'count' unless you specifically want to avg() in which case 'gauge'
            metrics = [{
                'metric': 'local.metric.today',
                'points': [q_count],
                'tags': ['env:stage'],
                'type': 'count'
            }]
            datadog.api.Metric.send(metrics=metrics)

        except:
            print("Failed to read from DB or write to Datadog")
            close_connection_and_cursor(con, cur)

        if DEBUG == True:
            print("Sleeping {0} after last fetch gave {1}".format(interval_seconds,q_count))
        
        try:
            time.sleep(interval_seconds)
        except KeyboardInterrupt:
            print("exiting loop as KeyboardInterrupt caught!")
            close_connection_and_cursor(con, cur)
            break

    if DEBUG is True or 1>0:
        print("During looped sampling achieved {0} requests".format(iter_count))

    exit(0)

