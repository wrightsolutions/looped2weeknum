#!/usr/bin/env python
#
#  Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from __future__ import with_statement

from os import path,sep
from datetime import datetime as dt
import re
import shlex
import subprocess
from sys import argv

from string import ascii_letters,digits,printable
#SET_LOWERUPPER_AND_DIGITS=set(''.join([ascii_lowercase,ascii_uppercase,digits]))
SET_LOWERUPPER_AND_DIGITS=set(ascii_letters).union(digits)
SET_PRINTABLE = set(printable)
#SET_PRINTABLE_NOT_PERIOD = set(printable).difference(set(chr(46)))

try:
    import sqlite3 as db3
except ImportError:
    print("Is sqlite (python-pysqlite1.1) installed?")
    #print("Is sqlite (sqlite3) installed? Import error {0}".format(e))
    """ apt-get install python-pysqlite1.1
    # http://archive.debian.net/lenny/python-pysqlite1.1
    """
    exit(191)

prog = argv[0].strip()
prog_basename = path.basename(prog)
prog_basename_extensionless = path.splitext(prog_basename)[0]

BUFFER_SIZE = 0
MEG_AS_FLOAT = float(2**10)

class ConnectionCursor( object ):
    """ ConnectionCursor object for given path, connection, cursor
    """

    ISOCOMPACT_STRFTIME='%Y%m%dT%H%M%S'
    #MEG_AS_FLOAT = float(2**20)
    MEG_AS_FLOAT = float(2**10)

    NAME_FROM_SQLITE_MASTER_TEMPLATE = "SELECT name FROM sqlite_master WHERE type='table' AND name='{0}';"
    PROCPERCENT_TABLENAME = 'procpercent'
    SQLSTRING_CREATE_PROC = ("CREATE TABLE IF NOT EXISTS"
                             " %s(pid integer NOT NULL,"
                             " cmdword text NOT NULL, "
                             " sampled_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                             " pcpu real NOT NULL,"
                             " cputime real NOT NULL,"
                             " started text NOT NULL,"
                             " mrss real NOT NULL,"
                             " msize real NOT NULL,"
                             " mvsize real NOT NULL,"
                             " user text NOT NULL,"
                             " status text NOT NULL,"
                             " cmdline text NOT NULL,"
                             " notes text,"
                             " PRIMARY KEY(pid,cmdword,sampled_at,pcpu,cputime)"
                             " );" % (PROCPERCENT_TABLENAME))

    def __init__(self, pattern='/bin/', pathstem='/tmp/', pathsep=None, debug=False):
        self._pattern = pattern
        self._pathstem = pathstem
        if pathsep is None:
            self._pathsep = sep
        else:
            self._pathsep = pathsep
        self._date_today_init = dt.today()
        # Note that date_today_init is fixed at class init time and never updated
        self._yyyyww = "%s%s" % (self._date_today_init.year,self._date_today_init.isocalendar()[1])
        # Similarly yyyyww is based on date_today_init and so again is fixed in time
        self._dbfilepath = None
        if self._pattern is not None:
            patred = re.sub(r'\W+', '', self._pattern)
            if debug:
                print("patred=%s from pattern=%s used to generate dbfilepath." % (patred,self._pattern))
            """ Above we have removed non-word characters from pattern and assigned result to
            pattern reduced (patred)
            """
            if len(patred) > 1:
                self._dbfilepath = "%s%sponesample25%sfields3__%s.sqlite" % (self._pathstem,
                                                                             self._pathsep,
                                                                             patred,self._yyyyww)
            else:
                if set(self._pattern).issubset(SET_LOWERUPPER_AND_DIGITS):
                    print("pattern does not consist wholly of letters and digits!")
                raise RuntimeError("pattern given did not contain enough word characters. Quitting")
        self._connection = None
        self._cursor = None
        self._schema = None
        self._tuples_list = []


    """ pathstem property requires no setter as only set directly during class init. """
    @property
    def pathstem(self):
        return self._pathstem

    def pathsep_get(self):
        return self._pathsep

    def pathsep_set(self,pathsep):
        self._pathsep = pathsep
        
    pathsep = property(pathsep_get,pathsep_set)

    """ date_today_init property requires no setter as only set directly during class init. """
    @property
    def date_today_init(self):
        return self._date_today_init

    """ yyyyww property requires no setter as only set directly during class init. """
    @property
    def yyyyww(self):
        return self._yyyyww

    """ dbfilepath requires no setter as we do not want to expose it as publicly settable. """
    @property
    def dbfilepath(self):
        return self._dbfilepath

    """ connection property requires no setter """
    @property
    def connection(self):
        return self._connection

    def connection_commit(self,cursor_drop=True,catch=True):
        """ Here it is the cursor to which we issue a commit, but name chosen as often
        we are coupled with functionality of connection_close() in terms of logical calls
        This default to cursor_drop=True and so if that is not what you want then you
        should set False or be using another vehicle for issuing your 'commit;'
        """
        if self.cursor is not None:
            try:
                self.cursor.execute("commit;")
                if cursor_drop is True:
                    self.cursor_close()
            except:
                if catch is not True:
                    raise
        return

    def connection_close(self):
        if self.connection:
            self.connection.close()
            self._connection = None
        return self.connection

    """ cursor property requires no setter as we do not want to expose it as publicly settable """
    @property
    def cursor(self):
        """ Access the underlying cursor field but do not attempt any on demand creation """
        return self._cursor

    """ schema property requires no setter as we do not want to expose it as publicly settable """
    @property
    def schema(self):
        """ This is a single table database. Once the table is created the string of the
        schema should be placed in this field by internal processing. Check this field is
        None or otherwise. If it contains anything other than None, then the table is available.
        """
        return self._schema


    def tuples_list_get(self):
        return self._tuples_list

    def tuples_list_set(self,tuples_list):
        self._tuples_list = tuples_list
        
    tuples_list = property(tuples_list_get,tuples_list_set)


    def cursor_close(self):
        if self.cursor:
            self.cursor.close()
            self._cursor = None
        return


    def cur(self,debug=False):
        """ Not to be confused with the property 'cursor', this method will create a cursor
        on demand if one does not exist.
        """

        if self.cursor:
            pass
        else:
            try:
                if self.connection:
                    cur = self._connection.cursor()
                    # Avoid adding any arguments to cursor() as this is external method
                    self._cursor = cur
            except:
                if debug: print("cur() failed to get a cursor")
                pass

        return self.cursor


    def cursor_execute(self,sql_statement,rows_expected=0,debug=False):

        executed_flag = False

        self.cur(debug)
        if self.cursor is None:
            return executed_flag

        try:
            if debug is True:
                print("Should have cursor and now will execute sql statement shown next.")
                print(sql_statement)
            self.cursor.execute(sql_statement)
            executed_flag = True
        except db3.ProgrammingError:
            #err_str = "Sqlite execute() ProgrammingError occurred: {0}".format(e)
            err_str = "Sqlite execute() ProgrammingError occurred."
            if debug: print(err_str)
            self.cursor_close()
            raise RuntimeError(err_str)
        except db3.Error:
            """
            err_str = "Sqlite execute() error {0}: {1}".format(
                database_path, e.args[0])
            """
            err_str = "Sqlite execute() error for db at %s" % self.dbfilepath
            if debug: print(err_str)
            self.cursor_close()
            raise RuntimeError(err_str)
        except:
            err_str = "Sqlite error during .execute() create if not exists."
            if debug: print(err_str)
            self.cursor_close()
            raise RuntimeError(err_str)

        if 0 == rows_expected:
            rowcount = 0
        elif 1 == rows_expected:
            rows = self.cursor.fetchone()
            rowcount = rows[0]
        else:
            rows = self.cursor.fetchall()
            rowcount = rows[0]

        if executed_flag:
            if rowcount == rows_expected:
                pass
            else:
                executed_flag = False

        return executed_flag


    def cursor_execute_all(self,debug=False):

        executed_flag = False

        self.cur(debug)
        #if self.cursor is None:
        #    return executed_flag

        if debug is True:
            print("iterating list of length %s inserting each into the table" % len(self.tuples_list))

        if len(self.tuples_list) < 1:
            return executed_flag

        if debug is True:
            print("iterating list of length %s inserting each into the table" % len(self.tuples_list))

        #tmatching_list.append((pcpu,pid,time,mrss,msize,mvsize,started,owner,
        #                       status,cmdline,before,after,line,notes))


        for tup in self.tuples_list:
            try:
                if debug is True:
                    print("Should have cursor and now will execute INSERT statement next.")
                self.cursor.execute("""INSERT INTO procpercent (pid,cmdword,sampled_at,pcpu,cputime,
started,mrss,msize,mvsize,user,status,cmdline,notes)
VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);
""",tup)
                executed_flag = True
            except db3.OperationalError:
                #err_str = 'Sqlite execute() OperationalError occurred: {0}'.format(e)
                err_str = 'Sqlite execute() OperationalError occurred.'
                self.cursor_close()
                if debug: print(err_str)
            except db3.ProgrammingError:
                #err_str = "Sqlite execute() ProgrammingError occurred: {0}".format(e)
                err_str = "Sqlite execute() ProgrammingError occurred."
                if debug: print(err_str)
                self.cursor_close()
                raise RuntimeError(err_str)
            except db3.Error:
                #err_str = "Sqlite execute() error {0}: {1}".format(database_path, e.args[0])
                err_str = "Sqlite execute() error for db at %s" % self.dbfilepath
                if debug: print(err_str)
                self.cursor_close()
                raise RuntimeError(err_str)
            except ValueError:
                #err_str = 'Sqlite insert failed ValueError: {0}'.format(e)
                err_str = 'Sqlite insert failed ValueError.'
                self.cursor_close()
                if debug: print(err_str)
            except Exception:
                #err_str = 'Sqlite insert failed: {0}'.format(e)
                err_str = 'Sqlite insert failed with Exception.'
                self.cursor_close()
                if debug: print(err_str)
                raise
            except:
                err_str = "Sqlite error during .execute() create if not exists."
                if debug: print(err_str)
                self.cursor_close()
                raise RuntimeError(err_str)

        return executed_flag


    def create_table_procpercent(self,debug=False):
        """ Having a cursor object does not guarantee that the file was an sqlite
        database. A plain text file get a cursor, but fail on cur.execute() """

        if debug:
            print(ConnectionCursor.SQLSTRING_CREATE_PROC)

        self.cur()
        created_flag = self.cursor_execute(ConnectionCursor.SQLSTRING_CREATE_PROC,0,debug)
        if created_flag is True:
            self._schema = ConnectionCursor.SQLSTRING_CREATE_PROC

        return created_flag


    def connection_cursor(self,debug=False):

        if self.dbfilepath is None:
            return None

        ready_marker = 2

        if self.connection is None:

            ready_marker = 0

            if path.exists(self.dbfilepath):
                # Sqlite file exists but we have no connection?
                pass

            try:
                dbcon = db3.connect(self.dbfilepath,timeout=2)
                ready_marker += 1
            except db3.Error:
                #except db3.Error as e:
                #print("Sqlite connection error occurred: {0}".format(e.args[0]))
                err_str = "Sqlite connection Error occurred."
                err_str = "%s Do you have write access to %s ?" % (err_str,self._pathstem)
                if debug: print(err_str)
                raise RuntimeError(err_str)
            except Exception:
                err_str = "Sqlite connection Exception occurred"
                if debug: print(err_str)
                raise RuntimeError(err_str)

            if dbcon:
                dbcon.isolation_level = None
                self._connection = dbcon

                created_flag = self.create_table_procpercent(debug)
                if created_flag is True:
                    ready_marker += 1
                else:
                    self.connection_close()

        if debug is True:
            print("connection_cursor() ready_marker=%s" % ready_marker)

        if ready_marker > 1:
            """ Either we just did a connnect and table create or else we
            already had a connection in first place
            """
            try:
                #cur = self._connection.cursor()
                self.cur()
                if debug and self.cursor: print("connection_cursor() has given a cursor.")
            except:
                # self._cursor = None
                self.cursor_close()

        return self.cursor


def sorted_floats(list_original,reverse=False):
    list_floats = [ float(item) for item in list_original ]
    return sorted(list_floats,reverse=reverse)


def process_lines(stdout_unsplit,pattern,start_field=0,double_zero=False,zombies=False):
    """ Default is to discard lines that are double_zero so have 0 rss and 0 size
    however if zombies is set True then that overrides the above behaviour
    ProcessInfo = namedtuple('ProcessInfo', 
    ['pcpu','pid','cputime','mrss','msize','mvsize','started','user',
    'status','args','before','after','line','notes'])
    """
    tmatching_list = []
    for idx,line in enumerate(stdout_unsplit.splitlines()):

        line_array = line.split()
        pcpu = line_array[0]
        try:
            rss = line_array[3]
            mrss = int(rss)
        except:
            err_str = "Error processing ps output line %s" % idx
            err_str = "%s having pcpu=%s" % (err_str,pcpu)
            err_str = "%s and line=%s" % (err_str,line)
            raise RuntimeError(err_str)
        try:
            size = line_array[4]
            msize = int(size)
            vsize = line_array[5]
            mvsize = int(vsize)
        except:
            err_str = "Error processing ps output line %s" % idx
            err_str = "%s having pcpu=%s" % (err_str,pcpu)
            err_str = "%s and line=%s" % (err_str,line)
            raise RuntimeError(err_str)
        """ size is total program size (pages)
        size is text+data+stack
        """

        status = line_array[8]
        #print(line,mrss,msize)
        if double_zero is not True and 0 == sum([mrss,msize]):
            """ We have been instructed to discard lines
            where rss and size are both zero. We will use 'continue'
            to exit this iteration of the loop
            """
            if zombies is True and 'Z' in status:
                pass
            else:
                continue

        if prog_basename in line:
            # Exclude ourself (this program) from possibility of matching
            continue

        started = line_array[6]
        owner = line_array[7]

        matches0 = re.match(("(.*)(%s\s+%s)(.*)" % (started,owner)),line)
        if matches0:
            before = matches0.group(0)
            started_owner = matches0.group(1)
            after = matches0.group(2)
        else:
            raise RuntimeError("Error during line splitting using started+owner")

        pattern_grouped = "(.*)(%s)(.*)" % pattern
        #print(pattern_grouped)
        matches = re.search(pattern_grouped,line)
        if matches:
            before = matches.group(0)
            pattern_matched = matches.group(2)
            after = matches.group(3)
            notes = "[%s]%s" % (pattern_matched,after.rstrip())
        else:
            continue

        pid = line_array[1]
        time = line_array[2]
        cmdline = ' '.join(line_array[9:])
        tmatching_list.append((pcpu,pid,time,mrss,msize,mvsize,started,owner,
                               status,cmdline,before,after,line,notes))

    return tmatching_list


def process_filter(pattern,start_field):

    process_out = None
    lines = []

    pscmd_as_list = ['/bin/ps','-eo','pcpu,pid,time,rss,size,vsize,start_time,user,stat,args','--no-headers']
    #print(pscmd)
    """
    pscmd = "/bin/ps -eo pcpu,pid,time,rss,size,vsize,start_time,user,stat,args --no-headers | {0}".format(sortcmd)
    psproc = subprocess.Popen(shlex.split(pscmd),bufsize=BUFFER_SIZE,
                              stdout=subprocess.PIPE)
    """
    psproc = subprocess.Popen(pscmd_as_list,bufsize=BUFFER_SIZE,
                              stdout=subprocess.PIPE)
    sortcmd = "/usr/bin/sort -b -nk1 -k5 -r"
    psort = subprocess.Popen(shlex.split(sortcmd),bufsize=BUFFER_SIZE,
                             stdin=psproc.stdout,stdout=subprocess.PIPE)

    """ psproc.communicate()
    When chaining pipes of popen we only call the final .communicate() rather than
    each point in the chain. If you issue more than one .communicate() then your
    results may be unpredictable
    """
    psortout, psorterr = psort.communicate()
    psproc.stdout.close()  # Allow pdirs to receive a SIGPIPE if pgrep exits.
    psort.stdout.close()  # Allow pgrep to receive a SIGPIPE if psummer exits.
    psort_rc = psort.returncode

    if 0 == psort_rc and psortout:
        process_out = psortout
    else:
        print(psort_rc,psortout)
        print(psorterr)
        raise RuntimeError("Error during ps command call %s" % pscmd)

    tuples_matching = process_lines(process_out,pattern,start_field,False,False)

    pcpu_list = []

    for tup in tuples_matching:
        #print(procinfo.line)
        pcpu_list.append(tup[0])
        lines.append(tup[12])

    if 0 == len(pcpu_list):
        print("No pcpu list to report as no lines matching pattern=%s" % pattern)
    else:
        pcpu_list_sorted = sorted_floats(pcpu_list,reverse=True)
        list_sorted = [ str(item) for item in pcpu_list_sorted ]
        isoformat_now = dt.now().isoformat()
        line1 = "%s %s for pattern=%s" % (isoformat_now,' '.join(list_sorted),pattern)
        print(line1)
            
    return tuples_matching



if __name__ == "__main__":

    if (len(argv) < 2):
        exit(131)

    pathstem_given = '/tmp/'
    if len(argv) > 2:
        pathstem = argv[2]
        if pathstem.startswith('/var/') or pathstem.startswith('/opt/'):
            pathstem_given = pathstem

    exit_rc = 0

    pattern_given = '/sbin/'

    DEBUG = False

    if len(argv) > 1:
        pattern_given = argv[1]

    try:
        tuples_matching = process_filter(pattern_given,0)
    except:
        exit_rc = 141
        raise

    if 0 == exit_rc and len(tuples_matching) < 1:
        exit_rc = 150

    if 0 == exit_rc:
        if DEBUG is True:
            print(".sqlite file will be created in %s as requested." % pathstem_given)
        cc = ConnectionCursor(pattern=pattern_given,pathstem=pathstem_given,debug=DEBUG)
        if DEBUG is True:
            print("pattern=%s was used to generate dbfilepath=%s" % (pattern_given,cc.dbfilepath))
            print("tuples_matching of length %s will be given to ConnectionCursor" % len(tuples_matching))
        #tmatching_list.append((pcpu,pid,time,mrss,msize,mvsize,started,owner,
        #                       status,cmdline,before,after,line,notes))
        """INSERT INTO procpercent (pid,cmdword,sampled_at,pcpu,cputime,
started,mrss,msize,mvsize,user,status,cmdline,notes)
VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);
"""
        sampled_at = dt.now().isoformat()
        tuples_list = []
        for tup in tuples_matching:
            mrss = tup[3] / MEG_AS_FLOAT
            msize = tup[4] / MEG_AS_FLOAT
            mvsize = tup[5] / MEG_AS_FLOAT
            cmdline = tup[9]
            cmdword = cmdline.split()[0]
            # Next we take the tuple with extra field and lose one plus reorder
            # so fields are suitable for table insert
            tup13 = (tup[1],cmdword,sampled_at,tup[0],tup[2],tup[6],mrss,msize,mvsize,tup[7],
                     tup[8],cmdline,tup[13])
            tuples_list.append(tup13)
        cc.tuples_list = tuples_list
        cur = cc.connection_cursor(DEBUG)
        inserted_flag = cc.cursor_execute_all(DEBUG)
        if inserted_flag is True:
            pass
        else:
            exit_rc = 161

        try:
            cc.connection_commit()
            #print("Closing sqlite db at path {0}".format(cc.dbfilepath))
            cc.connection_close()
        except:
            pass

    exit(exit_rc)


